$(function(){

    $(document).on("click", ".likeButton", function(){

        var post_id = $(this).data("postid");
        var target_link = $(this).data("likeurl");
        var formData = 'id=' + post_id;

        $(this).hide();
        $("#loadLikeImg" + post_id).show();

        $.ajax({
            url: target_link,
            data: formData,
            dataType: "JSON",
            method: "POST",
            beforeSend: function(xhr){
                xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            },
            success:function(data){
                if (data.status == '1') {
                    $("#loadLikeImg" + post_id).hide();            
                    $("#buttonGroup" + post_id).load(" #buttonGroup" + post_id);
                } else {
                    $(this).show();
                    $("#loadLikeImg" + post_id).hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $(this).show();
                $("#loadLikeImg" + post_id).hide();
            }
        });

    });

    $(document).on("click", ".unlikeButton", function(){

        var post_id = $(this).data("postid");
        var unlike_id = $(this).data("unlikeid");
        var target_link = $(this).data("unlikeurl");
        var formData = 'id=' + unlike_id;

        $(this).hide();
        $("#loadLikeImg" + post_id).show();

        $.ajax({
            url: target_link,
            data: formData,
            dataType: "JSON",
            method: "POST",
            beforeSend: function(xhr){
                xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            },
            success:function(data){
                if (data.status == '1') {
                    $("#loadLikeImg" + post_id).hide();            
                    $("#buttonGroup" + post_id).load(" #buttonGroup" + post_id);
                } else {
                    $(this).show();
                    $("#loadLikeImg" + post_id).hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $(this).show();
                $("#loadLikeImg" + post_id).hide();
            }
        });

    });

    $(document).on("click", ".repostButton", function(){

        var post_id = $(this).data("postid");
        var target_link = $(this).data("reposturl");
        var formData = 'id=' + post_id;

        $(this).hide();
        $("#loadRepostImg" + post_id).show();

        $.ajax({
            url: target_link,
            data: formData,
            dataType: "JSON",
            method: "POST",
            beforeSend: function(xhr){
                xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            },
            success:function(data){
                $(window).scrollTop(0);
                window.location.reload(true);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $(window).scrollTop(0);
                window.location.reload(true);
                console.log(xhr.responseText);
            }
        });

    });

    $(document).on("click", ".followButton", function(){

        var post_id = $(this).data("postid");
        var follow_id = $(this).data("followid");
        var target_link = $(this).data("followurl");
        var formData = 'id=' + follow_id;

        $(this).hide();
        $("#loadFollowImg" + post_id).show();

        $.ajax({
            url: target_link,
            data: formData,
            dataType: "JSON",
            method: "POST",
            beforeSend: function(xhr){
                xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            },
            success:function(data){
                if (data.status == '1') {
                    $("#loadFollowImg" + post_id).hide();            
                    $("#followGroup" + post_id).load(" #followGroup" + post_id);
                } else {
                    $(this).show();
                    $("#loadFollowImg" + post_id).hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $(this).show();
                $("#loadFollowImg" + post_id).hide();
            }
        });

    });

    $(document).on("click", ".unfollowButton", function(){

        var post_id = $(this).data("postid");
        var follow_id = $(this).data("followid");
        var target_link = $(this).data("unfollowurl");
        var formData = 'id=' + follow_id;

        $(this).hide();
        $("#loadFollowImg" + post_id).show();

        $.ajax({
            url: target_link,
            data: formData,
            dataType: "JSON",
            method: "POST",
            beforeSend: function(xhr){
                xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            },
            success:function(data){
                if (data.status == '1') {
                    $("#loadFollowImg" + post_id).hide();            
                    $("#followGroup" + post_id).load(" #followGroup" + post_id);
                } else {
                    $(this).show();
                    $("#loadFollowImg" + post_id).hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                $(this).show();
                $("#loadFollowImg" + post_id).hide();
            }
        });

    });

    $(document).on("click", ".deletepostButton", function(){

        var post_id = $(this).data("postid");
        var target_link = $(this).data("deleteurl");
        var formData = 'id=' + post_id;
        
        $(this).hide();
        $("#loadDeleteImg" + post_id).show();

        if (confirm("Are you sure you want to delete this post?")) {
            $.ajax({
                url: target_link,
                data: formData,
                dataType: "JSON",
                method: "POST",
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRF-Token", csrfToken);
                },
                success:function(data){
                    $(window).scrollTop(0);
                    window.location.replace(homeUrl);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $(window).scrollTop(0);
                    window.location.replace(homeUrl);
                    console.log(xhr.responseText);
                }
            });

        } else {
            $(this).show();
            $("#loadDeleteImg" + post_id).hide();
        }

    });


    $(document).on("click", ".deletecommentButton", function(){

        var comment_id = $(this).data("commentid");
        var target_link = $(this).data("deleteurl");
        var formData = 'id=' + comment_id;
        
        $(this).hide();
        $("#loadDeleteImg" + comment_id).show();

        if (confirm("Are you sure you want to delete this comment?")) {
            $.ajax({
                url: target_link,
                data: formData,
                dataType: "JSON",
                method: "POST",
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRF-Token", csrfToken);
                },
                success:function(data){
                    $(window).scrollTop(0);
                    window.location.replace(homeUrl);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $(window).scrollTop(0);
                    window.location.replace(homeUrl);
                    console.log(xhr.responseText);
                }
            });

        } else {
            $(this).show();
            $("#loadDeleteImg" + comment_id).hide();
        }

    });

    $("#deactivateUser").on("click", function(){

        var user_id = $(this).data("id");
        var formData = 'id=' + user_id;
        var target_link = $(this).data("deactivateurl");

        if (confirm("Are you sure you want to deactivate this account?")) {
            $.ajax({
                url: target_link,
                data: formData,
                dataType: "JSON",
                method: "POST",
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRF-Token", csrfToken);
                },
                success:function(data){
                    $(window).scrollTop(0);
                    window.location.replace(homeUrl);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $(window).scrollTop(0);
                    window.location.reload(true);
                    console.log(xhr.responseText);
                }
            });

        } else {
            $(window).scrollTop(0);
            window.location.reload(true);
        }

    });

});