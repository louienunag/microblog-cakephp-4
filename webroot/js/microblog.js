var csrfToken = $('meta[name="csrfToken"]').attr('content');

$(function(){

    $('#searchPosts').on("click", function(){
        $('#searchType').val('Posts');
        $('#searchForm').submit();
    });

    $('#searchUsers').on("click", function(){
        $('#searchType').val('Users');
        $('#searchForm').submit();
    });

});

function filePreview(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage').empty();
            $('#previewImage').html('Preview: <br />');
        	var image = $('<img>')
        			.attr('src', e.target.result)
        			.width('25%')
        			.height('25%');
    		$(image).appendTo('#previewImage');
    		$('#previewImage').after('<br />');
        };

        reader.readAsDataURL(input.files[0]);

    }

}