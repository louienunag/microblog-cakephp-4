<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
/** @var \Cake\Routing\RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    /*
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, templates/Pages/home.php)...
     *
     * $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
     *
     * ...and connect the rest of 'Pages' controller's URLs.
     *
     * $builder->connect('/pages/*', 'Pages::display');
    */

    /*
     * Connect catchall routes for all controllers.
     *
     * The `fallbacks` method is a shortcut for
     *
     * ```
     * $builder->connect('/:controller', ['action' => 'index']);
     * $builder->connect('/:controller/:action/*', []);
     * ```
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     *
     * $builder->fallbacks();
     */
    
    $builder->connect('/', ['controller' => 'Home', 'action' => 'index']);

    $builder->connect('/login', ['controller' => 'Login', 'action' => 'login']);
    $builder->connect('/logout', ['controller' => 'Login', 'action' => 'logout']);
    $builder->connect('/register', ['controller' => 'Login', 'action' => 'register']);
    $builder->connect('/dispatch', ['controller' => 'Login', 'action' => 'dispatch']);
    $builder->connect('/activation/:id', ['controller' => 'Login', 'action' => 'activation'], ["pass" => ["id"]]);
    $builder->connect('/activate-email', ['controller' => 'Login', 'action' => 'activateEmail']);
    $builder->connect('/reactivation/:id', ['controller' => 'Login', 'action' => 'reactivation'], ["pass" => ["id"]]);
    $builder->connect('/reset-email', ['controller' => 'Login', 'action' => 'resetEmail']);
    $builder->connect('/reset-password/:id', ['controller' => 'Login', 'action' => 'resetPassword'], ["pass" => ["id"]]);

    $builder->connect('/home', ['controller' => 'Home', 'action' => 'index']);
    $builder->connect('/search', ['controller' => 'Home', 'action' => 'search']);
    $builder->connect('/post/:id', ['controller' => 'Home', 'action' => 'view'], ["pass" => ["id"]]);
    $builder->connect('/post/edit/:id', ['controller' => 'Home', 'action' => 'edit'], ["pass" => ["id"]]);
    $builder->connect('/post/delete', ['controller' => 'Home', 'action' => 'delete']);
    $builder->connect('/comment/edit/:id', ['controller' => 'Home', 'action' => 'editComment'], ["pass" => ["id"]]);
    $builder->connect('/comment/delete', ['controller' => 'Home', 'action' => 'deleteComment']);

    $builder->connect('/like-post', ['controller' => 'Home', 'action' => 'likePost']);
    $builder->connect('/unlike-post', ['controller' => 'Home', 'action' => 'unlikePost']);
    $builder->connect('/retweet-post', ['controller' => 'Home', 'action' => 'retweetPost']);
    $builder->connect('/follow-user', ['controller' => 'Home', 'action' => 'followUser']);
    $builder->connect('/unfollow-user', ['controller' => 'Home', 'action' => 'unfollowUser']);

    $builder->connect('/profile', ['controller' => 'Profile', 'action' => 'index']);
    $builder->connect('/profile/view/:id', ['controller' => 'Profile', 'action' => 'view'], ["pass" => ["id"]]);
    $builder->connect('/profile/edit/:id', ['controller' => 'Profile', 'action' => 'edit'], ["pass" => ["id"]]);
    $builder->connect('/profile/delete', ['controller' => 'Profile', 'action' => 'delete']);
    $builder->connect('/followers', ['controller' => 'Profile', 'action' => 'followers']);
    $builder->connect('/following', ['controller' => 'Profile', 'action' => 'following']);


    $builder->fallbacks();

});

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * $routes->scope('/api', function (RouteBuilder $builder) {
 *     // No $builder->applyMiddleware() here.
 *     
 *     // Parse specified extensions from URLs
 *     // $builder->setExtensions(['json', 'xml']);
 *     
 *     // Connect API actions here.
 * });
 * ```
 */
