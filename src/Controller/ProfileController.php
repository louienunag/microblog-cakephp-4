<?php
declare(strict_types=1);

namespace App\Controller;

class ProfileController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("Users");
        $this->loadModel("Posts");
        $this->loadModel("Followers");
        $this->loadModel("Likes");
        $this->viewBuilder()->setLayout('mainLayout');
    }
    
    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();
        
        $userDeleted = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();

        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $newPost = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($newPost, 'add');
        if ($this->request->is('post')) {
            $postData = $this->request->getData();
            $postData['user_id'] = $user_id;
            $postData['image'] = null;
            $newPost = $this->Posts->patchEntity($newPost, $postData);

            $upload = $this->request->getUploadedFile('image');            
            if ($upload !== null && $upload->getError() !== \UPLOAD_ERR_NO_FILE) {
                $image = $this->request->getData('image');
                $filename = rand(1000, 9999) . $image->getClientFilename();
                $filetype = $image->getClientMediaType();
                $filepath = WWW_ROOT . 'img' . DS . 'uploads' . DS . $filename;
                if ($filetype == 'image/jpeg' || $filetype == 'image/jpg' || $filetype == 'image/png') {
                    if (!empty($filename)) {
                        if ($image->getSize() > 0 && $image->getError() == 0) {
                            $image->moveTo($filepath); 
                            $newPost->image = $filename;
                        }
                    }
                }  
            }

            if ($this->Posts->save($newPost)) {
                $this->Flash->success(__('Successfully Posted.', [
                    'clear' => true
                ]));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Failed to post.', [
                'clear' => true
            ]));
        }

        $userPosts = $this->Posts 
            ->find()
            ->contain(['Users'])
            ->where([['Posts.deleted IS' => NULL], ['Posts.user_id = (:user_id)']])
            ->bind(':user_id', $user_id, 'string')
            ->order(['Posts.id' => 'DESC', 'Posts.modified' => 'DESC']);
        $this->Authorization->authorize($userPosts, 'index');
        $viewPosts = $user->applyScope('index', $userPosts);
        
        $this->set("posts", $this->paginate($viewPosts, ['limit' => '5']));

        $viewPosts = $viewPosts->toArray();
        foreach ($viewPosts as $key => $posts) {
            $liked = $this->Likes
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'post_id = (:post_id)'])
                ->bind(':user_id', $user_id, 'string')
                ->bind(':post_id', $posts['id'], 'string')
                ->first();
            
            $followed = $this->Followers
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'follower_id = (:follow_id)'])
                ->bind(':user_id', $user_id, 'string')
                ->bind(':follow_id', $posts['user_id'], 'string')
                ->first();

            $viewPosts[$key]['like_id'] = $liked['id'];
            $viewPosts[$key]['follow_id'] = $followed['id'];
        }

        $likeCount = $this->Likes
            ->find()
            ->where(['user_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->count();

        $followerCount = $this->Followers
            ->find()
            ->where(['follower_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->count();

        $followingCount = $this->Followers
            ->find()
            ->where(['user_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->count();

        $query = $this->Users->find();
        $query->select([  
                'image_profile',
                'created', 
                'user_name' => $query->func()->concat([
                    'first_name' => 'identifier', 
                    ' ', 
                    'middle_name' => 'identifier', 
                    ' ', 
                    'last_name' => 'identifier'
                ]) 
            ])
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string');
        $userData = $query->first();
        $this->Authorization->authorize($userData, 'view');

        $this->set("title", "Profile Page");
        $this->set("id", $user_id);
        $this->set(compact("user_id"));
        $this->set(compact("likeCount"));
        $this->set(compact("followerCount"));
        $this->set(compact("followingCount"));
        $this->set(compact("userData"));
        $this->set(compact("newPost"));
    }

    public function view($id)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();
        
        $userDeleted = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();

        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $userPosts = $this->Posts 
            ->find()
            ->contain(['Users'])
            ->where([['Posts.deleted IS' => NULL], ['Posts.user_id = (:user_id)']])
            ->bind(':user_id', $id, 'string')
            ->order(['Posts.id' => 'DESC', 'Posts.modified' => 'DESC']);
        $this->Authorization->authorize($userPosts, 'index');
        $viewPosts = $user->applyScope('index', $userPosts);
        
        $this->set("posts", $this->paginate($viewPosts, ['limit' => '5']));

        $viewPosts = $viewPosts->toArray();
        foreach ($viewPosts as $key => $posts) {
            $liked = $this->Likes
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'post_id = (:post_id)'])
                ->bind(':user_id', $id, 'string')
                ->bind(':post_id', $posts['id'], 'string')
                ->first();
            
            $followed = $this->Followers
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'follower_id = (:follow_id)'])
                ->bind(':user_id', $id, 'string')
                ->bind(':follow_id', $posts['user_id'], 'string')
                ->first();

            $viewPosts[$key]['like_id'] = $liked['id'];
            $viewPosts[$key]['follow_id'] = $followed['id'];
        }

        $likeCount = $this->Likes
            ->find()
            ->where(['user_id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->count();

        $followerCount = $this->Followers
            ->find()
            ->where(['follower_id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->count();

        $followingCount = $this->Followers
            ->find()
            ->where(['user_id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->count();

        $query = $this->Users->find();
        $query->select([  
                'image_profile',
                'created', 
                'user_name' => $query->func()->concat([
                    'first_name' => 'identifier', 
                    ' ', 
                    'middle_name' => 'identifier', 
                    ' ', 
                    'last_name' => 'identifier'
                ]) 
            ])
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string');
        $userData = $query->first();
        $this->Authorization->authorize($userData, 'view');

        $this->set("title", "Profile Page");
        $this->set(compact("id"));
        $this->set(compact("user_id"));
        $this->set(compact("likeCount"));
        $this->set(compact("followerCount"));
        $this->set(compact("followingCount"));
        $this->set(compact("userData"));
        $this->set(compact("newPost"));
    }

    public function edit($id)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();
        
        $userData = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->first();

        $this->Authorization->authorize($userData);

        if ($userData->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $editUser = $this->Users->patchEntity($userData, $this->request->getData());

            if (!$editUser->getErrors()) {
                $upload = $this->request->getUploadedFile('edit_image');            
                if ($upload !== null && $upload->getError() !== \UPLOAD_ERR_NO_FILE) {
                    $image = $this->request->getData('edit_image');
                    $filename = rand(1000, 9999) . $image->getClientFilename();
                    $filetype = $image->getClientMediaType();
                    $filepath = WWW_ROOT . 'img' . DS . 'uploads' . DS . $filename;
                    if ($filetype == 'image/jpeg' || $filetype == 'image/jpg' || $filetype == 'image/png') {
                        if (!empty($filename)) {
                            if ($image->getSize() > 0 && $image->getError() == 0) {
                                $image->moveTo($filepath); 
                                $editUser->image_profile = $filename;
                            }
                        }
                    }  
                }
            }

            if ($this->Users->save($editUser)) {
                $this->Flash->success(__('Successfully Updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Failed to update.'));
        }

        $this->set("title", "Edit Profile");
        $this->set(compact('user_id'));
        $this->set(compact('userData'));
    }

    public function delete()
    {
        $user = $this->request->getAttribute('identity');
        $id = $this->request->getData('id');

        $userData = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->first();

        $this->Authorization->authorize($userData);

        $deleteUser = $this->Users->patchEntity($userData, array('deleted' => date('Y-m-d'), 'activated' => null));
        if ($this->Users->save($deleteUser)) {
            $this->Flash->success(__('Successfully Deactivated.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 1,
                "message" => "Successfully Deactivated."
            )); 

            exit;

        } else {
            $this->Flash->error(__('Failed to deactivate.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 0,
                "message" => "Please, try again."
            )); 

            exit;

        }   
    }

    public function followers()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $userFollowers = $this->Followers 
            ->find()
            ->join([
                'table' => 'users',
                'alias' => 'Users',
                'type' => 'INNER',
                'conditions' => 'Users.id = follower_id',
            ])
            ->where(['Followers.follower_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->order(['Followers.id' => 'DESC']);
        $this->Authorization->authorize($userFollowers, 'followers');
        $viewFollowers = $user->applyScope('followers', $userFollowers);

        $followerCount = $this->Followers 
            ->find()
            ->where(['Followers.follower_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->count();

        $this->set("title", "Followers");
        $this->set(compact('user_id'));
        $this->set(compact('followerCount'));
        $this->set("users", $this->paginate($viewFollowers, ['limit' => '5']));
    }

    public function following()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $userFollowing = $this->Followers 
            ->find()
            ->contain(['Users'])
            ->where(['Followers.user_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')

            ->order(['Followers.id' => 'DESC']);
        $this->Authorization->authorize($userFollowing, 'following');
        $viewFollowing = $user->applyScope('following', $userFollowing);

        $followingCount = $this->Followers 
            ->find()
            ->where(['Followers.user_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->count();

        $this->set("title", "Following");
        $this->set(compact('user_id'));
        $this->set(compact('followingCount'));
        $this->set("users", $this->paginate($viewFollowing, ['limit' => '5']));
    }
}

?>