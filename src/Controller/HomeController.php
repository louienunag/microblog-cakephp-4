<?php
declare(strict_types=1);

namespace App\Controller;

class HomeController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("Users");
        $this->loadModel("Posts");
        $this->loadModel("Comments");
        $this->loadModel("Followers");
        $this->loadModel("Likes");
        $this->viewBuilder()->setLayout('mainLayout');
    }

    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $userDeleted = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();

        $this->Authorization->authorize($userDeleted, 'view');

        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $newPost = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($newPost, 'add');
        if ($this->request->is('post')) {
            $postData = $this->request->getData();
            $postData['user_id'] = $user_id;
            $postData['image'] = null;
            $newPost = $this->Posts->patchEntity($newPost, $postData);

            $upload = $this->request->getUploadedFile('image');            
            if ($upload !== null && $upload->getError() !== \UPLOAD_ERR_NO_FILE) {
                $image = $this->request->getData('image');
                $filename = rand(1000, 9999) . $image->getClientFilename();
                $filetype = $image->getClientMediaType();
                $filepath = WWW_ROOT . 'img' . DS . 'uploads' . DS . $filename;
                if ($filetype == 'image/jpeg' || $filetype == 'image/jpg' || $filetype == 'image/png') {
                    if (!empty($filename)) {
                        if ($image->getSize() > 0 && $image->getError() == 0) {
                            $image->moveTo($filepath); 
                            $newPost->image = $filename;
                        }
                    }
                }  
            }

            if ($this->Posts->save($newPost)) {
                $this->Flash->success(__('Successfully Posted.', [
                    'clear' => true
                ]));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Failed to post.', [
                'clear' => true
            ]));
        }

        $followingPosts = $this->Followers
            ->find()
            ->select(['follower_id'])
            ->where(['user_id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string');
        $userPosts = $this->Posts 
            ->find()
            ->contain(['Users'])
            ->where([
                'Posts.deleted IS' => NULL,
                'Users.deleted IS' => NULL,
                'OR' => [['Posts.user_id = (:user_id)'], ['Posts.user_id IN' => $followingPosts]]
            ])
            ->bind(':user_id', $user_id, 'string')
            ->order(['Posts.id' => 'DESC', 'Posts.modified' => 'DESC']);
        $this->Authorization->authorize($userPosts, 'index');
        $viewPosts = $user->applyScope('index', $userPosts);

        $this->set("posts", $this->paginate($viewPosts, ['limit' => '5']));

        $viewPosts = $viewPosts->toArray();
        foreach ($viewPosts as $key => $posts) {
            $liked = $this->Likes
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'post_id = (:post_id)'])
                ->bind(':user_id', $user_id, 'string')
                ->bind(':post_id', $posts['id'], 'string')
                ->first();
            
            $followed = $this->Followers
                ->find()
                ->select(['id'])
                ->where(['user_id = (:user_id)', 'follower_id = (:follow_id)'])
                ->bind(':user_id', $user_id, 'string')
                ->bind(':follow_id', $posts['user_id'], 'string')
                ->first();

            $viewPosts[$key]['like_id'] = $liked['id'];
            $viewPosts[$key]['follow_id'] = $followed['id'];
        }

        $this->set("title", "Homepage");
        $this->set(compact("user_id"));
        $this->set(compact("newPost"));
    }

    public function search()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $search_type = $this->request->getQuery('searchType');
        $keyword = '%' . $this->request->getQuery('keyword') . '%';

        if ($search_type == "Posts") {

            $followingPosts = $this->Followers
                ->find()
                ->select(['follower_id'])
                ->where(['user_id = (:user_id)'])
                ->bind(':user_id', $user_id, 'string');
            $searchPosts = $this->Posts
                ->find()
                ->contain(['Users'])
                ->where([
                    'Users.deleted IS' => NULL, 
                    'Posts.deleted IS' => NULL, 
                    'Posts.content LIKE (:keyword)',
                    'OR' => [['Posts.user_id = (:user_id)'], ['Posts.user_id IN' => $followingPosts]],
                ])
                ->bind(':keyword', $keyword, 'string')
                ->order(['Posts.id' => 'DESC', 'Posts.modified' => 'DESC']);
            $this->Authorization->authorize($searchPosts, 'search');
            $query = $user->applyScope('search', $searchPosts);

            $this->set("posts", $this->paginate($searchPosts, ['limit' => '5']));

            $searchPosts = $searchPosts->toArray();
            foreach ($searchPosts as $key => $posts) {
                $liked = $this->Likes
                    ->find()
                    ->select(['id'])
                    ->where(['user_id = (:user_id)', 'post_id = (:post_id)'])
                    ->bind(':user_id', $user_id, 'string')
                    ->bind(':post_id', $posts['id'], 'string')
                    ->first();
                
                $followed = $this->Followers
                    ->find()
                    ->select(['id'])
                    ->where(['user_id = (:user_id)', 'follower_id = (:follow_id)'])
                    ->bind(':user_id', $user_id, 'string')
                    ->bind(':follow_id', $posts['user_id'], 'string')
                    ->first();

                $searchPosts[$key]['like_id'] = $liked['id'];
                $searchPosts[$key]['follow_id'] = $followed['id'];
            }

        } elseif ($search_type == "Users") {
            $searchUsers = $this->Users
                ->find()
                ->where([
                    'id != (:user_id)',
                    'deleted IS' => NULL,
                    'OR' => [
                        ['username LIKE (:keyword)'], 
                        ['first_name LIKE (:keyword)'], 
                        ['middle_name LIKE (:keyword)'], 
                        ['last_name LIKE (:keyword)']
                    ]
                ])
                ->bind(':user_id', $user_id, 'string')
                ->bind(':keyword', $keyword, 'string')
                ->order(['id' => 'DESC', 'modified' => 'DESC']);
            $this->Authorization->authorize($searchUsers, 'search');
            $query = $user->applyScope('search', $searchUsers);

            $this->set("users", $this->paginate($searchUsers, ['limit' => '5']));

            $searchUsers = $searchUsers->toArray();
            foreach ($searchUsers as $key => $users) {
                $followed = $this->Followers
                    ->find()
                    ->select(['id'])
                    ->where(['user_id = (:user_id)', 'follower_id = (:follow_id)'])
                    ->bind(':user_id', $user_id, 'string')
                    ->bind(':follow_id', $users['user_id'], 'string')
                    ->first();

                $searchUsers[$key]['follow_id'] = $followed['id'];
            }

        } else {
            $this->Authorization->skipAuthorization();
        }

        $this->set("title", "Search Page");
        $this->set(compact("user_id"));
        $this->set(compact("search_type"));
        $this->set('keyword', $this->request->getQuery('keyword'));
    }

    public function view($id)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $newComment = $this->Comments->newEmptyEntity();
        $this->Authorization->authorize($newComment, 'add');
        if ($this->request->is('post')) {
            $postData = $this->request->getData();
            $postData['user_id'] = $user_id;
            $postData['post_id'] = $id;
            $newComment = $this->Comments->patchEntity($newComment, $postData);

            if ($this->Comments->save($newComment)) {
                $this->Flash->success(__('Comment Added Successfully.', [
                    'clear' => true
                ]));
                return $this->redirect(['action' => 'view', 'id' => $id]);
            }
            $this->Flash->error(__('Failed to comment.', [
                'clear' => true
            ]));
        }

        $userComments = $this->Comments
            ->find()
            ->contain(['Users'])
            ->where(['Comments.deleted IS' => NULL, 'Comments.post_id = (:post_id)'])
            ->bind(':post_id', $id, 'string')
            ->order(['Comments.id' => 'DESC', 'Comments.modified' => 'DESC']);
        $this->Authorization->authorize($userComments, 'view');
        $viewComments = $user->applyScope('view', $userComments);

        $query = $this->Posts->find();
        $query
            ->select([
                'id', 
                'user_id', 
                'content', 
                'image',  
                'image_profile' => 'Users.image_profile', 
                'user_name' => $query->func()->concat([
                    'Users.first_name' => 'identifier', 
                    ' ', 
                    'Users.middle_name' => 'identifier', 
                    ' ', 
                    'Users.last_name' => 'identifier'
                ]), 
            ])
            ->contain(['Users'])
            ->where(['Posts.deleted IS' => NULL, 'Posts.id = (:post_id)'])
            ->bind(':post_id', $id, 'string');
        $viewPost = $query->first();
        $this->Authorization->authorize($viewPost, 'view');

        $like_id = $this->Likes
            ->find()
            ->select(['id'])
            ->where(['user_id = (:user_id)', 'post_id = (:post_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->bind(':post_id', $viewPost->id, 'string')
            ->first();

        $follow_id = $this->Followers
            ->find()
            ->select(['id'])
            ->where(['user_id = (:user_id)', 'follower_id  = (:follow_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->bind(':follow_id', $viewPost->user_id, 'string')
            ->first();

        $this->set("title", "View Post");
        $this->set(compact("user_id"));
        $this->set("post_id", $id);
        $this->set("posts", $viewPost);
        $this->set("like_id", $like_id['id']);
        $this->set("follow_id", $follow_id['id']);
        $this->set(compact("newComment"));
        $this->set("comments", $this->paginate($viewComments, ['limit' => '5']));
    }

    public function edit($id)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();
        
        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }

        $editPost = $this->Posts
            ->find('all')
            ->where(['Posts.deleted IS' => NULL, 'id = (:post_id)'])
            ->bind(':post_id', $id, 'string')
            ->first();

        $this->Authorization->authorize($editPost);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $editPost = $this->Posts->patchEntity($editPost, $this->request->getData());

            if (!$editPost->getErrors()) {
                $upload = $this->request->getUploadedFile('edit_image');            
                if ($upload !== null && $upload->getError() !== \UPLOAD_ERR_NO_FILE) {
                    $image = $this->request->getData('edit_image');
                    $filename = rand(1000, 9999) . $image->getClientFilename();
                    $filetype = $image->getClientMediaType();
                    $filepath = WWW_ROOT . 'img' . DS . 'uploads' . DS . $filename;
                    if ($filetype == 'image/jpeg' || $filetype == 'image/jpg' || $filetype == 'image/png') {
                        if (!empty($filename)) {
                            if ($image->getSize() > 0 && $image->getError() == 0) {
                                $image->moveTo($filepath); 
                                $editPost->image = $filename;
                            }
                        }
                    }  
                }
            }

            if ($this->Posts->save($editPost)) {
                $this->Flash->success(__('Successfully Updated.', [
                    'clear' => true
                ]));
                return $this->redirect(['action' => 'view', 'id' => $id]);
            }
            $this->Flash->error(__('Failed to post.', [
                'clear' => true
            ]));
        }

        $this->set("title", "Edit Post");
        $this->set("post_id", $id);
        $this->set("posts", $editPost);
    }

    public function delete()
    {
        $user = $this->request->getAttribute('identity');
        $id = $this->request->getData('id');

        $postData = $this->Posts
            ->find('all')
            ->where(['id = (:post_id)'])
            ->bind(':post_id', $id, 'string')
            ->first();
            
        $this->Authorization->authorize($postData);

        $deletePost = $this->Posts->patchEntity($postData, array('deleted' => date('Y-m-d')));
        if ($this->Posts->save($deletePost)) {
            $this->Flash->success(__('Successfully Deleted.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 1,
                "message" => "You deleted the post."
            )); 

            exit;

        } else {
            $this->Flash->error(__('Failed to delete.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 0,
                "message" => "Please, try again."
            )); 

            exit;

        }   
    }


    public function editComment($id)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();
        
        $userDeleted = $this->Users
            ->find('all')
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $user_id, 'string')
            ->first();
        
        $this->Authorization->authorize($userDeleted, 'view');
            
        if ($userDeleted->deleted) {
            $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Login', 'action' => 'login']);
        }
        
        $editComment = $this->Comments
            ->find('all')
            ->where(['deleted IS' => NULL, 'id = (:comment_id)'])
            ->bind(':comment_id', $id, 'string')
            ->first();
            
        $this->Authorization->authorize($editComment, 'edit');
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $editComment = $this->Comments->patchEntity($editComment, $this->request->getData());

            if ($this->Comments->save($editComment)) {
                $this->Flash->success(__('Successfully Updated.', [
                    'clear' => true
                ]));
                return $this->redirect(['action' => 'view', 'id' => $editComment->post_id]);
            }
            $this->Flash->error(__('Failed to post.', [
                'clear' => true
            ]));
        }

        $this->set("title", "Edit Comment");
        $this->set("comment_id", $id);
        $this->set("comments", $editComment);
    }

    public function deleteComment()
    {
        $user = $this->request->getAttribute('identity');
        $id = $this->request->getData('id');

        $commentData = $this->Comments
            ->find('all')
            ->where(['id = (:comment_id)'])
            ->bind(':comment_id', $id, 'string')
            ->first();

        $this->Authorization->authorize($commentData, 'delete');

        $deleteComment = $this->Comments->patchEntity($commentData, array('deleted' => date('Y-m-d')));
        if ($this->Comments->save($deleteComment)) {
            $this->Flash->success(__('Successfully Deleted.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 1,
                "message" => "You deleted the comment."
            )); 

            exit;

        } else {
            $this->Flash->error(__('Failed to delete.', [
                'clear' => true
            ]));

            echo json_encode(array(
                "status" => 0,
                "message" => "Please, try again."
            )); 

            exit;

        }   
    }

    public function likePost()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $this->Authorization->skipAuthorization();

        $newLike = $this->Likes->newEmptyEntity();        
        if ($this->request->is('ajax')) {
            $postData['user_id'] = $user_id;
            $postData['post_id'] = $this->request->getData('id');
            $newLike = $this->Likes->patchEntity($newLike, $postData);

            if ($this->Likes->save($newLike)) {
                echo json_encode(array(
                    "status" => 1,
                    "message" => "You liked the post."
                )); 
    
                exit;

            } else {
                echo json_encode(array(
                    "status" => 0,
                    "message" => "Please, try again."
                )); 
    
                exit;

            }   
        }
    }

    public function unlikePost()
    {
        $this->Authorization->skipAuthorization();

        if ($this->request->is('ajax')) {
            $id = $this->request->getData("id");

            $likeData = $this->Likes
                ->find()
                ->where(['id = (:like_id)'])
                ->bind(':like_id', $id, 'string')
                ->first();

            if ($this->Likes->delete($likeData)) {
                echo json_encode(array(
                    "status" => 1,
                    "message" => "You unliked the post."
                )); 
    
                exit;

            } else {
                echo json_encode(array(
                    "status" => 0,
                    "message" => "Please, try again."
                )); 
    
                exit;
            }   
        }
    }

    public function retweetPost()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $this->Authorization->skipAuthorization();

        $newPost = $this->Posts->newEmptyEntity();
        if ($this->request->is('ajax')) {
            $id = $this->request->getData("id");

            $oldData = $this->Posts
                ->find('all')
                ->where(['id = (:post_id)'])
                ->bind(':post_id', $id, 'string')
                ->first();
                
            $newData['user_id'] = $user_id;
            $newData['content'] = $oldData->content;
            $newData['image'] = $oldData->image;

            $newPost = $this->Posts->patchEntity($newPost, $newData, ['validate' => false]);
            if ($this->Posts->save($newPost)) {
                $this->Flash->success(__('Successfully Reposted.', [
                    'clear' => true
                ]));
                
                echo json_encode(array(
                    "status" => 1,
                    "message" => "You reposted the post."
                )); 
    
                exit;

            } else {

                $this->Flash->error(__('Failed to repost.', [
                    'clear' => true
                ]));

                echo json_encode(array(
                    "status" => 0,
                    "message" => "Please, try again."
                )); 
    
                exit;
            }   
        }
    }

    public function followUser()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->getIdentifier();

        $this->Authorization->skipAuthorization();

        $newFollower = $this->Followers->newEmptyEntity();        
        if ($this->request->is('ajax')) {
            $postData['user_id'] = $user_id;
            $postData['follower_id'] = $this->request->getData('id');
            $newFollower = $this->Followers->patchEntity($newFollower, $postData); 

            if ($this->Followers->save($newFollower)) {
                echo json_encode(array(
                    "status" => 1,
                    "message" => "You followed a user."
                )); 
   
                 exit;

            } else {
                echo json_encode(array(
                    "status" => 0,
                    "message" => $newFollower->getErrors()
                )); 
    
                exit;

            }   
        }
    }

    public function unfollowUser()
    {
        $this->Authorization->skipAuthorization();

        if ($this->request->is('ajax')) {
            $id = $this->request->getData("id");
            
            $followData = $this->Followers
                ->find('all')
                ->where(['id = (:follow_id)'])
                ->bind(':follow_id', $id, 'string')
                ->first();
        
            if ($this->Followers->delete($followData)) {
                echo json_encode(array(
                    "status" => 1,
                    "message" => "You unfollowed a user."
                )); 
    
                exit;

            } else {
                echo json_encode(array(
                    "status" => 0,
                    "message" => "Please, try again."
                )); 
    
                exit;
            }   
        }
    }
}

?>