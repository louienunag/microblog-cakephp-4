<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;

class LoginController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel("Users");
        $this->viewBuilder()->setLayout('mainLayout');
    }

    public function login()
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $user = $this->request->getAttribute('identity');
            $user_id = $user->getIdentifier();

            $userDeleted = $this->Users
                ->find()
                ->where(['id = (:user_id)'])
                ->bind(':user_id', $user_id, 'string')
                ->first();
            
            $this->Authorization->authorize($userDeleted, 'view');
            
            if (!$userDeleted->deleted) {
                return $this->redirect(['controller' => 'Home', 'action' => 'index']);

            } else {
                $this->Flash->error(__('User Deactivated. Please reactivate your account.'));
                $this->Authentication->logout();
                return $this->redirect(['action' => 'login']);
            }

        }

        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            debug($result);
            $this->Flash->error(__('Invalid username or password'));

        }

        $this->set("title", "Microblog Login");
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['action' => 'login']);

        }
    }

    public function register()
    {
        $this->Authorization->skipAuthorization();

        $users = $this->Users->newEmptyEntity();

        if ($this->request->is('post')) {
            $users = $this->Users->patchEntity($users, $this->request->getData());

            if ($this->Users->save($users)) {
                $id = $users['id'];
                $name = $users['first_name']; 
                $emailTo = $users['email'];

                $mailer = new Mailer();
                $mailer->setEmailFormat('html')
                    ->setTo($emailTo)
                    ->setViewVars([
                        'name' => $name, 
                        'id' => $id
                    ])
                    ->viewBuilder()
                        ->setTemplate('register');
                $mailer->deliver();

                $this->Flash->success(__('Successfully Registered.'));
                return $this->redirect(['action' => 'dispatch']);

            }
            $this->Flash->error(__('Failed to register.'));

        }

        $this->set("title", "Microblog Signup");
        $this->set(compact('users'));
    }

    public function dispatch()
    {
        $this->Authorization->skipAuthorization();
        
        $this->set("title", "Email Verification");
    }  

    public function activation($id)
    {
        $this->Authorization->skipAuthorization();

        $users = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $formdata = $this->request->getData();
            $formdata['activated'] = date('Y-m-d');
            
            $users = $this->Users->patchEntity($users, $formdata);
            if ($this->Users->save($users)) {
                $this->Flash->success(__('Successfully Activated. Please login to continue.'));
                return $this->redirect(['action' => 'login']);

            }
            $this->Flash->error(__('Activation Failed.'));

        }

        $this->set("title", "Email Activation");
        $this->set(compact('users'));
    }

    public function resetEmail()
    {
        $this->Authorization->skipAuthorization();

        if ($this->request->is('post')) {
            $emailTo = $this->request->getData('email');

            $users = $this->Users
                ->find()
                ->where(['email = (:email)'])
                ->bind(':email', $emailTo, 'string')
                ->first();

            if ($users) {
                $id = $users['id'];
                $name = $users['first_name']; 
                $emailTo = $users['email'];

                $mailer = new Mailer();
                $mailer->setEmailFormat('html')
                    ->setTo($emailTo)
                    ->setViewVars([
                        'name' => $name, 
                        'id' => $id
                    ])
                    ->viewBuilder()
                        ->setTemplate('reset');
                $mailer->deliver();

                $this->Flash->success(__('Successfully Sent.'));
                return $this->redirect(['action' => 'dispatch']);

            }
            $this->Flash->error(__('Failed to send. Try Again.'));

        }

        $this->set("title", "Reset Password");
    }

    public function resetPassword($id)
    {
        $this->Authorization->skipAuthorization();

        $users = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $formdata = $this->request->getData();
            
            $users = $this->Users->patchEntity($users, $formdata);
            if ($this->Users->save($users)) {
                $this->Flash->success(__('Password Successfully Updated. Please login to continue.'));
                return $this->redirect(['action' => 'login']);

            }
            $this->Flash->error(__('Password Update Failed.'));

        }

        $this->set("title", "Reset Password");
        $this->set(compact('users'));
    }


    public function activateEmail()
    {
        $this->Authorization->skipAuthorization();

        if ($this->request->is('post')) {
            $emailTo = $this->request->getData('email');

            $users = $this->Users
                ->find()
                ->where(['email = (:email)'])
                ->bind(':email', $emailTo, 'string')
                ->first();

            if ($users['activated']) {
                $id = $users['id'];
                $name = $users['first_name']; 
                $emailTo = $users['email'];

                $mailer = new Mailer();
                $mailer->setEmailFormat('html')
                    ->setTo($emailTo)
                    ->setViewVars([
                        'name' => $name, 
                        'id' => $id
                    ])
                    ->viewBuilder()
                        ->setTemplate('activate');
                $mailer->deliver();

                $this->Flash->success(__('Successfully Sent.'));
                return $this->redirect(['action' => 'dispatch']);

            }
            $this->Flash->error(__('Failed to send. Try Again.'));

        }

        $this->set("title", "Email Activation");
    }

    public function reactivation($id)
    {
        $this->Authorization->skipAuthorization();

        $users = $this->Users
            ->find()
            ->where(['id = (:user_id)'])
            ->bind(':user_id', $id, 'string')
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $formdata = $this->request->getData();
            $formdata['activated'] = date('Y-m-d');
            $formdata['deleted'] = null;
            
            $users = $this->Users->patchEntity($users, $formdata);
            if ($this->Users->save($users)) {
                $this->Flash->success(__('Successfully Activated. Please login to continue.'));
                return $this->redirect(['action' => 'login']);

            }
            $this->Flash->error(__('Activation Failed.'));

        }

        $this->set("title", "Email Activation");
        $this->set(compact('users'));
    }
}

?>