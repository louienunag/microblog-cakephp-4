<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Followers;
use Authorization\IdentityInterface;

/**
 * Followers policy
 */
class FollowersPolicy
{
    /**
     * Check if $user can add Followers
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Followers $followers
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Followers $followers)
    {
        return true;
    }

    /**
     * Check if $user can edit Followers
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Followers $followers
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Followers $followers)
    {
        return true;
    }

    /**
     * Check if $user can delete Followers
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Followers $followers
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Followers $followers)
    {
        return true;
    }

    /**
     * Check if $user can view Followers
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Followers $followers
     * @return bool
     */
    public function canView(IdentityInterface $user, Followers $followers)
    {
        return true;
    }
}
