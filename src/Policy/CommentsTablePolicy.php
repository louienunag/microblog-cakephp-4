<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\CommentsTable;
use Authorization\IdentityInterface;

/**
 * Comments policy
 */
class CommentsTablePolicy
{
	public function canView($user, $posts)
    {
        return true;
    }

    public function scopeView($user, $query)
    {
        return $query->select([
            'comment_id' => 'Comments.id',
            'content',
            'profile_id' => 'Users.id', 
            'image_profile' => 'Users.image_profile', 
            'username' => 'Users.username', 
            'user_name' => $query->func()->concat([
                'Users.first_name' => 'identifier', 
                ' ', 
                'Users.middle_name' => 'identifier', 
                ' ', 
                'Users.last_name' => 'identifier'
            ])
        ]);
    }
}
