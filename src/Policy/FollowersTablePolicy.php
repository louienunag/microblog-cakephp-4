<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\FollowersTable;
use Authorization\IdentityInterface;

/**
 * Followers policy
 */
class FollowersTablePolicy
{
    public function canFollowers($user, $followers)
    {
        return true;
    }

    public function scopeFollowers($user, $query)
    {
        return $query->select([
            'follow_id' => 'Followers.id', 
            'image_profile' => 'Users.image_profile', 
            'username' => 'Users.username', 
            'user_name' => $query->func()->concat([
                'Users.first_name' => 'identifier', 
                ' ', 
                'Users.middle_name' => 'identifier', 
                ' ', 
                'Users.last_name' => 'identifier'
            ]),
        ]);
    }

    public function canFollowing($user, $followers)
    {
        return true;
    }

    public function scopeFollowing($user, $query)
    {
        return $query->select([
            'follow_id' => 'Followers.id', 
            'image_profile' => 'Users.image_profile', 
            'username' => 'Users.username', 
            'user_name' => $query->func()->concat([
                'Users.first_name' => 'identifier', 
                ' ', 
                'Users.middle_name' => 'identifier', 
                ' ', 
                'Users.last_name' => 'identifier'
            ]),
        ]);
    }
}
