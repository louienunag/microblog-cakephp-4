<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\UsersTable;
use Authorization\IdentityInterface;

/**
 * Users policy
 */
class UsersTablePolicy
{
    /**
     * Check if $user can view User
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Table\UsersTable $users
     * @return bool
     */
    public function canView(IdentityInterface $user, $users)
    {
        return true;
    }

    /**
     * Check if $user can search User
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Table\UsersTable $users
     * @return bool
     */
    public function canSearch(IdentityInterface $user, $users)
    {
        return true;
    }

    /**
     * Returns $query in Search
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param Cake\ORM\Query $query
     * @return result
     */
    public function scopeSearch(IdentityInterface $user, $query)
    {
        return $query->select(['user_id' => 'id', 'image_profile' => 'image_profile', 'username', 'deleted',
            'user_name' => $query->func()->concat(['first_name' => 'identifier', ' ',
            'middle_name' => 'identifier', ' ', 'last_name' => 'identifier']),]);
    }
}
