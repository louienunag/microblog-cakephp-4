<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Like;
use Authorization\IdentityInterface;

/**
 * Like policy
 */
class LikePolicy
{
    /**
     * Check if $user can add Like
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Like $like
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Like $like)
    {
        return true;
    }

    /**
     * Check if $user can edit Like
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Like $like
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Like $like)
    {
        return $this->isAuthor($user, $comment);
    }

    /**
     * Check if $user can delete Like
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Like $like
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Like $like)
    {
        return $this->isAuthor($user, $comment);
    }

    /**
     * Check if $user can view Like
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Like $like
     * @return bool
     */
    public function canView(IdentityInterface $user, Like $like)
    {
        return true;
    }

    protected function isAuthor(IdentityInterface $user, Like $like)
    {
        return $like->user_id === $user->getIdentifier();
    }

}
