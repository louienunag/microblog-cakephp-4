<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\PostsTable;
use Authorization\IdentityInterface;

/**
 * Posts policy
 */
class PostsTablePolicy
{

    public function canIndex($user, $posts)
    {
        return true;
    }

    public function scopeIndex($user, $query)
    {
        return $query->select([
            'id', 
            'user_id', 
            'content', 
            'image',  
            'deleted',  
            'image_profile' => 'Users.image_profile',
            'username' => 'Users.username', 
            'user_name' => $query->func()->concat([
                'Users.first_name' => 'identifier', 
                ' ', 
                'Users.middle_name' => 'identifier', 
                ' ', 
                'Users.last_name' => 'identifier'
            ]),
        ]);
    }

    public function canSearch($user, $posts)
    {
        return true;
    }

    public function scopeSearch($user, $query)
    {
        return $query->select([
            'id', 
            'user_id', 
            'content', 
            'image',  
            'deleted',  
            'image_profile' => 'Users.image_profile', 
            'username' => 'Users.username', 
            'user_name' => $query->func()->concat([
                'Users.first_name' => 'identifier', 
                ' ', 
                'Users.middle_name' => 'identifier', 
                ' ', 
                'Users.last_name' => 'identifier'
            ]),
        ]);
    }

    public function canView($user, $posts)
    {
        return true;
    }
}
