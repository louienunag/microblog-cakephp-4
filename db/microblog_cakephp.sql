-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 19, 2021 at 12:38 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog_cakephp`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `follower_id`) VALUES
(1, 1, 3),
(2, 3, 1),
(3, 1, 4),
(4, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `image` text,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `content`, `image`, `created`, `modified`, `deleted`) VALUES
(1, 1, 'asdsd', 'sample_img.jpg', '2021-06-24', '2021-06-24', NULL),
(2, 3, 'another user', NULL, '2021-06-24', '2021-06-24', NULL),
(3, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(4, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(5, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(6, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(7, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(8, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(9, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(10, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(11, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(12, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(13, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(14, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(15, 1, 'asdsd', NULL, '2021-06-24', '2021-06-24', NULL),
(16, 1, 'try', 'R06d6c8b5417f76960895c7121f53e128.jpg', '2021-07-16', '2021-07-16', NULL),
(17, 1, 'hahah naganda', '186456212_944371849677313_6520697970427056249_n.jpg', '2021-07-16', '2021-07-16', NULL),
(18, 1, 'covid', 'COVIDDECLARATIONFORM.pdf', '2021-07-16', '2021-07-16', NULL),
(19, 1, 'aseacrh', '', '2021-07-16', '2021-07-16', NULL),
(20, 1, 'aseacrh', '', '2021-07-16', '2021-07-16', NULL),
(21, 1, 'aseacrh', '', '2021-07-16', '2021-07-16', NULL),
(22, 1, 'cat', 'R06d6c8b5417f76960895c7121f53e128.jpg', '2021-07-16', '2021-07-16', NULL),
(23, 1, 'aw', 'COVIDDECLARATIONFORM.pdf', '2021-07-16', '2021-07-16', NULL),
(24, 1, 'aw', 'COVIDDECLARATIONFORM.pdf', '2021-07-16', '2021-07-16', NULL),
(25, 1, 'aw', 'R06d6c8b5417f76960895c7121f53e128.jpg', '2021-07-16', '2021-07-16', NULL),
(26, 1, 'try', '1274R06d6c8b5417f76960895c7121f53e128.jpg', '2021-07-16', '2021-07-16', NULL),
(27, 1, 'try', '', '2021-07-16', '2021-07-16', NULL),
(28, 1, 'try', '', '2021-07-16', '2021-07-16', NULL),
(29, 1, 'walang picture to', '', '2021-07-16', '2021-07-16', NULL),
(30, 1, 'walang picture to', '', '2021-07-16', '2021-07-16', NULL),
(31, 1, 'try', '', '2021-07-16', '2021-07-16', NULL),
(32, 1, 'try', '', '2021-07-16', '2021-07-16', NULL),
(33, 1, 'try', '', '2021-07-16', '2021-07-16', NULL),
(34, 1, 'tryyyyyyyyyyyy', NULL, '2021-07-16', '2021-07-16', NULL),
(35, 1, 'gagana to pramis', NULL, '2021-07-16', '2021-07-16', NULL),
(36, 1, 'edit mo to pre', '79563174186456212_944371849677313_6520697970427056249_n.jpg', '2021-07-16', '2021-07-16', NULL),
(37, 1, 'gusto ko ng dice', '3406random-dice.jpg', '2021-07-16', '2021-07-16', NULL),
(38, 4, 'my cute cat', '1187210187313_336683948083518_666447322927192790_n.jpg', '2021-07-16', '2021-07-16', NULL),
(39, 4, 'at rancho sanluis resort montalban rizal', '8095200708534_954777802009232_2172113501886791596_n.jpg', '2021-07-16', '2021-07-16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` text,
  `image_profile` varchar(255) DEFAULT NULL,
  `activated` date DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `birthdate`, `username`, `password`, `image_profile`, `activated`, `created`, `modified`, `deleted`) VALUES
(1, 'louie', 'louie', 'louie', 'jovert.borger@gmail.com', '2021-06-24', 'louie', '$2y$10$0TevVQkOeMJ6DRHy3VRxq.5gqWXOhmgmz5yHXdksAA.w8GSc/slOy', 'sample_avatar.png', '2021-06-10', '2021-06-10', '2021-06-10', NULL),
(3, 'louie', 'manaog', 'nunag', 'louienunag.yns@gmail.com', '2021-06-01', 'marky', '$2y$10$sdb0uKiVz8u/VjYT5cmvM.CfWr3Aah0ZTS/bVlWx0R7Jw1G9VSl.K', '', '2021-06-24', '2021-06-23', '2021-06-24', NULL),
(4, 'emiru', 'chan', 'ikuno', 'maryann.empenio029@gmail.com', '1998-03-29', 'emiru.ikuno', '$2y$10$e4WcWbPEeLqDXm6IocvkIO8JI.V/iJLKCebRQ6rN0DjZPfsMrr1eO', NULL, '2021-06-25', '2021-06-25', '2021-06-25', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
