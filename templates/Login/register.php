<div class="w3-container blogTheme">    
  <?= $this->Form->create($users) ?>
  <div class="login-container">
    <?= $this->Flash->render() ?>
    <h2 align="center">Create an Account</h2>
    <label class="login-label" for="first_name"><b>First Name</b></label>
    <input type="text" class="login-input" placeholder="Enter First Name" name="first_name" required>
    <?= ($this->Form->isFieldError('first_name')) ? $this->Form->error('first_name') : "" ?>
    <label class="login-label" for="middle_name"><b>Middle Name</b></label>
    <input type="text" class="login-input" placeholder="Enter Middle Name" name="middle_name" required>
    <?= ($this->Form->isFieldError('middle_name')) ? $this->Form->error('middle_name') : "" ?>
    <label class="login-label" for="last_name"><b>Last Name</b></label>
    <input type="text" class="login-input" placeholder="Enter Last Name" name="last_name" required>
    <?= ($this->Form->isFieldError('last_name')) ? $this->Form->error('last_name') : "" ?>
    <label class="login-label" for="birthdate"><b>Birthday</b></label>
    <input type="date" class="login-input" name="birthdate" required>
    <?= ($this->Form->isFieldError('birthdate')) ? $this->Form->error('birthdate') : "" ?>
    <label class="login-label" for="email"><b>Email</b></label>
    <input type="email" class="login-input" placeholder="Enter Email" name="email" required>
    <?= ($this->Form->isFieldError('email')) ? $this->Form->error('email') : "" ?>
    <button type="submit" class="register-button custom-button w3-margin-bottom">Register</button>
    <div class="w3-row w3-margin-bottom">
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'login'])) ?>" class="w3-button link-button custom-button w3-margin-bottom">Back to Login</a>
      </div>
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'resetEmail'])) ?>" class="w3-button custom-button link-button">Forgot Password</a>
      </div>
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'activateEmail'])) ?>" class="w3-button custom-button link-button">Reactivate Account</a>
      </div>
    </div>
  </div>
  <?= $this->Form->end() ?>
</div>