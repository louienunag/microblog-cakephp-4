<div class="w3-container blogTheme">  
  <?php 
    if (!$users['activated']) {
  ?>  
    <?= $this->Form->create($users) ?>
    <div class="login-container">
      <?= $this->Flash->render() ?>
      <h2 align="center">Register</h2>
      <label class="login-label" for="username"><b>Username</b></label>
      <input type="text" class="login-input" placeholder="Enter Username" name="username" required>
      <?= ($this->Form->isFieldError('username')) ? $this->Form->error('username') : "" ?>
      <label class="login-label" for="password"><b>Password</b></label>
      <input type="password" class="login-input" placeholder="Enter Password" name="password" required>
      <?= ($this->Form->isFieldError('password')) ? $this->Form->error('password') : "" ?>
      <label class="login-label" for="repeat_password"><b>Repeat Password</b></label>
      <input type="password" class="login-input" placeholder="Repeat Password" name="repeat_password" required>
      <?= ($this->Form->isFieldError('repeat_password')) ? $this->Form->error('repeat_password') : "" ?>
      <button type="submit" class="register-button custom-button">Save</button>
    </div>
    <?= $this->Form->end() ?>
  
  <?php } else { ?>
    <div class="login-container">
      <?= $this->Flash->render() ?>
      <h2 align="center">Account Already Activated.</h2>
      <p align="center">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'login'])) ?>" class="w3-button link-button custom-button w3-margin-bottom">Back to Login</a>
      </p>
    </div>
  
  <?php } ?>
</div>