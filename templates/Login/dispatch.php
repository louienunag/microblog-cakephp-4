<div class="w3-container blogTheme">    
  <div class="login-container">
    <?= $this->Flash->render() ?>
    <h2 align="center">Email Activation</h2>
    <h4 align="center">Check your email and click the link sent to you.</h4>
    <p align="center">
      <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'login'])) ?>" class="w3-button link-button custom-button w3-margin-bottom">Back to Login</a>
    </p>
  </div>
</div>