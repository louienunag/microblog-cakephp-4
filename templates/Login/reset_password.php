<div class="w3-container blogTheme">    
  <?= $this->Form->create($users) ?>
  <div class="login-container">
    <?= $this->Flash->render() ?>
    <h2 align="center">Change Password</h2>
    <label class="login-label" for="password"><b>Password</b></label>
    <input type="password" class="login-input" placeholder="Enter Password" name="password" required>
    <?= ($this->Form->isFieldError('password')) ? $this->Form->error('password') : "" ?>
    <label class="login-label" for="repeat_password"><b>Repeat Password</b></label>
    <input type="password" class="login-input" placeholder="Repeat Password" name="repeat_password" required>
    <?= ($this->Form->isFieldError('repeat_password')) ? $this->Form->error('repeat_password') : "" ?>
    <button type="submit" class="register-button custom-button">Save</button>
  </div>
  <?= $this->Form->end() ?>
</div>