<div class="w3-container blogTheme">    
  <?= $this->Form->create(null) ?>
  <div class="login-container">
    <?= $this->Flash->render() ?>
    <h2 align="center">Activate Account</h2>
    <label class="login-label" for="email"><b>Email</b></label>
    <input type="email" class="login-input" placeholder="Enter Email" name="email" required>
    <?= ($this->Form->isFieldError('email')) ? $this->Form->error('email') : "" ?>
    <button type="submit" class="register-button custom-button w3-margin-bottom">Submit</button>
    <p align="center">
      <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'login'])) ?>" class="w3-button link-button custom-button w3-margin-bottom">Back to Login</a>
    </p>
  </div>
  <?= $this->Form->end() ?>
</div>