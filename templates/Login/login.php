<div class="w3-container blogTheme">    
  <div class="login-container">
  <?= $this->Form->create(null) ?>
    <?= $this->Flash->render() ?>
    <h2 align="center">Login</h2>
    <label class="login-label" for="username"><b>Username</b></label>
    <input type="text" class="login-input" placeholder="Enter Username" name="username">
    <?= ($this->Form->isFieldError('username')) ? $this->Form->error('username') : "" ?>
    <label class="login-label" for="password"><b>Password</b></label>
    <input type="password" class="login-input" placeholder="Enter Password" name="password">  
    <?= ($this->Form->isFieldError('password')) ? $this->Form->error('password') : "" ?>
    <button type="submit" class="register-button custom-button w3-margin-bottom">Login</button>
    <div class="w3-row w3-margin-bottom">
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'register'])) ?>" class="w3-button custom-button link-button">Create New Account</a>
      </div>
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'resetEmail'])) ?>" class="w3-button custom-button link-button">Forgot Password</a>
      </div>
      <div class="w3-col m4 w3-center-align">
        <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'activateEmail'])) ?>" class="w3-button custom-button link-button">Reactivate Account</a>
      </div>
    </div>
  <?= $this->Form->end() ?>
  </div>
</div>