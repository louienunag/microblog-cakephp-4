<!-- Page Container -->
<div class="w3-container blogTheme">    
  <!-- The Grid -->
  <div class="w3-row">
    
    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Profile', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-reply"></i> Back</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div class="main-nav">    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h4>Following Count: <?= h($followingCount) ?></h4>
              <hr class="w3-clear">
              <?php 
                foreach ($users as $index => $row) { $index++;
              ?>
                <div class="w3-row">
                  <div class="w3-col m6 w3-left-align">
                    <?php 
                      $profile_picture = ($row['image_profile']) ? $row['image_profile'] : 'default_avatar.png';
                      echo $this->Html->image('uploads/' . h($profile_picture), ['class' => 'w3-left w3-circle w3-margin-right', 'style' => 'width:60px']); 
                    ?>
                    <h4><?= h($row['user_name']) ?></h4>
                    <small><?= h('@'.$row['username']) ?></small>
                  </div>
                  <div class="w3-col m6 w3-right-align">
                    <div class="w3-row">
                      <div id="followGroup<?= h($index) ?>">
                        <?php
                          if (!$row['follow_id']) {
                        ?>
                          <a id="followButton<?= h($index) ?>" href="javascript:void(0)" class="w3-button custom-button link-button followButton" data-postid="<?= h($index) ?>" data-followid="<?= h($row['user_id']) ?>" data-followurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'followUser'])) ?>"><i class="fa fa-plus"></i> Follow</a>
                        <?php
                          } else {
                        ?>
                          <a id="unfollowButton<?= h($index) ?>" href="javascript:void(0)" class="w3-button custom-button link-button unfollowButton" data-postid="<?= h($index) ?>" data-followid="<?= h($row['follow_id']) ?>" data-unfollowurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'unfollowUser'])) ?>"><i class="fa fa-check"></i> Followed</a>
                        <?php
                          }
                        ?>
                        <span id="loadFollowImg<?= h($index) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif'); ?></span>
                      </div>              
                    </div> 
                  </div>
                </div>
              <?php
                }
              ?>
              <br>

              <div class="w3-row">
                <div class="w3-col m4">
                  <ul class="pagination"><?= $this->Paginator->prev("Previous Posts") ?></ul>
                </div>
                <div class="w3-col m4">
                  <ul class="pagination"><?= $this->Paginator->counter('Page {{page}} of {{pages}}') ?></ul>
                </div>
                <div class="w3-col m4">
                  <ul class="pagination"><?= $this->Paginator->next("Next Posts") ?></ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <!-- End Middle Column -->
    
    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->

  <!-- End Grid -->
  </div>
<!-- End Page Container -->
</div>