<div class="w3-container blogTheme">   
  <div class="w3-row">

    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Profile', 'action' => 'index', 'id' => h($user_id)])); ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-reply"></i> Back</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div class="main-nav">    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding w3-margin-bottom">
              <?= $this->Form->create($userData, array('enctype'=>'multipart/form-data')) ?>
                <?= $this->Flash->render() ?>
                <h2 align="center">Update Account</h2>
                <label class="login-label" for="upload_image"><b>Profile Picture</b></label>
                <input type="file" id="upload_image" name="edit_image" class="login-input" accept="image/*" onchange="filePreview(this)">
                <div id="previewImage">
                <?php 
                  if ($userData['image_profile']) {
                ?>
                    Preview: <br />
                    <?= $this->Html->image('uploads/' . h($userData['image_profile']), ['style' => 'width: 50%; height: 50%;']); ?>
                <?php 
                  }  
                ?>
                </div>
                <br />
                <label class="login-label" for="first_name"><b>First Name</b></label>
                <input type="text" class="login-input" placeholder="Enter First Name" name="first_name" value="<?= h($userData['first_name']) ?>" required>
                <?= ($this->Form->isFieldError('first_name')) ? $this->Form->error('first_name') : "" ?>
                <label class="login-label" for="middle_name"><b>Middle Name</b></label>
                <input type="text" class="login-input" placeholder="Enter Middle Name" name="middle_name" value="<?= h($userData['middle_name']) ?>" required>
                <?= ($this->Form->isFieldError('middle_name')) ? $this->Form->error('middle_name') : "" ?>
                <label class="login-label" for="last_name"><b>Last Name</b></label>
                <input type="text" class="login-input" placeholder="Enter Last Name" name="last_name" value="<?= h($userData['last_name']) ?>" required>
                <?= ($this->Form->isFieldError('last_name')) ? $this->Form->error('last_name') : "" ?>
                <label class="login-label" for="birthdate"><b>Birthday</b></label>
                <input type="date" class="login-input" name="birthdate" value="<?= h(date('Y-m-d', strtotime($userData['birthdate']))) ?>" required>
                <?= ($this->Form->isFieldError('birthdate')) ? $this->Form->error('birthdate') : "" ?>
                <hr class="w3-clear">
                <label class="login-label" for="password"><b>New Password</b></label>
                <input type="password" class="login-input" placeholder="Enter Password" name="password">
                <?= ($this->Form->isFieldError('password')) ? $this->Form->error('password') : "" ?>
                <label class="login-label" for="repeat_password"><b>Repeat New Password</b></label>
                <input type="password" class="login-input" placeholder="Repeat Password" name="repeat_password">
                <?= ($this->Form->isFieldError('repeat_password')) ? $this->Form->error('repeat_password') : "" ?>
                <br />
                <div class="w3-row">
                  <div class="w3-col m6"> 
                    <button type="submit" class="w3-button custom-button w3-margin-bottom"><i class="fa fa-pencil"></i> Update Profile</button> 
                  </div>
                  <div class="w3-col m6 w3-right-align">
                    <button type="button" id="deactivateUser" class="w3-button custom-button w3-margin-bottom" data-id="<?= h($user_id) ?>" data-deactivateurl="<?= h($this->Url->build(['controller' => 'Profile', 'action' => 'delete'])); ?>"><i class="fa fa-user-times"></i> Deactivate Account</button>
                  </div>
                </div>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Middle Column -->

    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])); ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])); ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->
    
  </div>
</div>

<script type="text/javascript">
  var homeUrl = <?= json_encode(h($this->Url->build(['controller' => 'Login', 'action' => 'logout']))) ?>;
</script>