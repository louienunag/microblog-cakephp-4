<!-- Page Container -->
<div class="w3-container blogTheme">    
  <!-- The Grid -->
  <div class="w3-row">
    
    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Profile', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-user"></i> Profile</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div id="main-tab" class="main-nav">    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <?= $this->Form->create($newPost, array('enctype'=>'multipart/form-data')) ?>
                <?= $this->Flash->render() ?>
                <?= ($this->Form->isFieldError('content')) ? $this->Form->error('content') : ""; ?>
                <textarea name="content" placeholder="Share your thoughts.." class="w3-margin-bottom" required></textarea>
                <input type="file" id="upload_image" name="image" accept="image/*" onchange="filePreview(this)" hidden>
                <?= ($this->Form->isFieldError('image')) ? $this->Form->error('image') : ""; ?>
                <div id="previewImage"></div>
                <button type="button" class="w3-button custom-button" onclick="document.getElementById('upload_image').click();"><i class="fa fa-file-image-o"></i> Add Image</button> 
                <button type="submit" class="w3-button custom-button"><i class="fa fa-pencil"></i> Post</button> 
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
      <?php 
        foreach ($posts as $row) { 
      ?>
        <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
          <div class="w3-row">
            <div class="w3-col m6 w3-left-align">
              <?php 
                $profile_picture = ($row['image_profile']) ? $row['image_profile'] : 'default_avatar.png';
                echo $this->Html->image('uploads/' . h($profile_picture), ['class' => 'w3-left w3-circle w3-margin-right', 'style' => 'width:60px']); 
              ?>
              <h4><?= h($row['user_name']) ?></h4>
              <small><?= h('@'.$row['username']) ?></small>
            </div>
            <div class="w3-col m6 w3-right-align">
            <?php 
              if ($row['user_id'] == $user_id) {
            ?>
              <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($row['id'])])) ?>" class="w3-button custom-button link-button"><i class="fa fa-eye"></i> View</a> 
              <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'edit', 'id' => h($row['id'])])) ?>" class="w3-button custom-button link-button"><i class="fa fa-pencil"></i> Edit</a>
            <?php 
              } else { 
            ?>
              <div class="w3-row">
                <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($row['id'])])) ?>" class="w3-button custom-button link-button"><i class="fa fa-eye"></i> View</a>
                <div id="followGroup<?= h($row['id']) ?>">
                  <?php
                    if (!$row['follow_id']) {
                  ?>
                    <a id="followButton<?= h($row['id']) ?>" href="javascript:void(0)" class="w3-button custom-button link-button followButton" data-postid="<?= h($row['id']) ?>" data-followid="<?= h($row['user_id']) ?>" data-followurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'followUser'])) ?>"><i class="fa fa-plus"></i> Follow</a>
                  <?php
                    } else {
                  ?>
                    <a id="unfollowButton<?= h($row['id']) ?>" href="javascript:void(0)" class="w3-button custom-button link-button unfollowButton" data-postid="<?= h($row['id']) ?>" data-followid="<?= h($row['follow_id']) ?>" data-unfollowurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'unfollowUser'])) ?>"><i class="fa fa-check"></i> Followed</a>
                  <?php
                    }
                  ?>
                  <span id="loadFollowImg<?= h($row['id']) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>
                </div>              
              </div> 
            <?php 
              } 
            ?>
            </div>
          </div>
          <hr class="w3-clear">
          <div class="w3-container">
            <p><?= nl2br(h($row['content'])) ?></p>
            <div align="center">
              <?php 
                if ($row['image']) {
                  echo $this->Html->image('uploads/' . h($row['image']), ['class' => 'w3-margin-bottom', 'style' => 'width:50%']);
                }  
              ?>
            </div>
            <hr class="w3-clear">
            <div class="w3-row">
              <div id="buttonGroup<?= h($row['id']) ?>" class="w3-col m4 w3-left-align w3-margin-bottom">
                <?php
                  if (!$row['like_id']) {
                ?>
                  <a id="likeButton<?= h($row['id']) ?>" href="javascript:void(0)" class="w3-button custom-button link-button likeButton" data-postid="<?= h($row['id']) ?>" data-likeurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'likePost'])) ?>"><i class="fa fa-thumbs-o-up"></i> Like</a>
                <?php
                  } else {
                ?>
                  <a id="unlikeButton<?= h($row['id']) ?>" href="javascript:void(0)" class="w3-button custom-button link-button unlikeButton" data-postid="<?= h($row['id']) ?>" data-unlikeid="<?= h($row['like_id']) ?>" data-unlikeurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'unlikePost'])) ?>"><i class="fa fa-thumbs-up"></i> Liked</a>
                <?php
                  }
                ?>
                <span id="loadLikeImg<?= h($row['id']) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>              
              </div> 
              <div class="w3-col m4 w3-center-align">
                <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($row['id'])])) ?>" class="w3-button custom-button link-button"><i class="fa fa-comment"></i> Comment</a>
              </div> 
              <div class="w3-col m4 w3-right-align">
                <a id="repostButton<?= h($row['id']) ?>" href="javascript:void(0)" class="w3-button custom-button link-button repostButton" data-postid="<?= h($row['id']) ?>" data-reposturl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'retweetPost'])) ?>"><i class="fa fa-share"></i> Repost</a>
                <span id="loadRepostImg<?= h($row['id']) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>
              </div>
            </div> 
          </div> 
        </div> 
      <?php 
        } 
      ?>
      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <div class="w3-row">
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->prev("Previous Posts") ?></ul>
          </div>
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->counter('Page {{page}} of {{pages}}') ?></ul>
          </div>
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->next("Next Posts") ?></ul>
          </div>
        </div>
      </div>  
    </div>
    <!-- End Middle Column -->
    
    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->

  <!-- End Grid -->
  </div>
<!-- End Page Container -->
</div>