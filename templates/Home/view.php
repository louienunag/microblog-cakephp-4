<!-- Page Container -->
<div class="w3-container blogTheme">    
  <!-- The Grid -->
  <div class="w3-row">
    
    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Profile', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-user"></i> Profile</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div class="main-nav"> 
      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <div class="w3-row">
          <div class="w3-col m6 w3-left-align">
            <?php
              $profile_picture = ($posts['image_profile']) ? $posts['image_profile'] : 'default_avatar.png';
              echo $this->Html->image('uploads/' . h($profile_picture), ['class' => 'w3-left w3-circle w3-margin-right', 'style' => 'width:60px']); 
            ?>
            <h4><?= h($posts['user_name']) ?></h4>
            <small><?= h('@'.$posts['username']) ?></small>
          </div>
          <div class="w3-col m6 w3-right-align">
            <?php 
              if ($posts['user_id'] == $user_id) { 
            ?>
              <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'edit', 'id' => h($post_id)])) ?>" class="w3-button custom-button link-button"><i class="fa fa-pencil"></i> Edit</a>
            <?php 
              } else { 
            ?>
              <div class="w3-row">
                <div id="followGroup<?= h($post_id) ?>">
                  <?php
                    if (!$follow_id) {
                  ?>
                    <a id="followButton<?= h($post_id) ?>" href="javascript:void(0)" class="w3-button custom-button link-button followButton" data-postid="<?= h($post_id) ?>" data-followid="<?= h($posts['user_id']) ?>" data-followurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'followUser'])) ?>"><i class="fa fa-plus"></i> Follow</a>
                  <?php
                    } else {
                  ?>
                    <a id="unfollowButton<?= h($post_id) ?>" href="javascript:void(0)" class="w3-button custom-button link-button unfollowButton" data-postid="<?= h($post_id) ?>" data-followid="<?= $follow_id ?>" data-unfollowurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'unfollowUser'])) ?>"><i class="fa fa-check"></i> Followed</a>
                  <?php
                    }
                  ?>
                  <span id="loadFollowImg<?= h($post_id) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>
                </div>              
              </div> 
            <?php 
              } 
            ?>
          </div>
        </div>
        <hr class="w3-clear">
        <div class="w3-container">
          <?= $this->Flash->render() ?>
          <p><?= nl2br(h($row['content'])) ?></p>
          <div align="center">
            <?php 
              if ($posts['image']) {
                echo $this->Html->image('uploads/' . h($posts['image']), ['class' => 'w3-margin-bottom', 'style' => 'width:50%']);
              } 
            ?>
          </div>
          <hr class="w3-clear">
          <div class="w3-row">
            <div id="buttonGroup<?= h($post_id) ?>" class="w3-col m6 w3-left-align w3-margin-bottom">
              <?php
                if (!$like_id) {
              ?>
                <a id="likeButton<?= h($post_id) ?>" href="javascript:void(0)" class="w3-button custom-button link-button likeButton" data-postid="<?= h($post_id) ?>" data-likeurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'likePost'])) ?>"><i class="fa fa-thumbs-o-up"></i> Like</a>
              <?php
                } else {
              ?>
                <a id="unlikeButton<?= h($post_id) ?>" href="javascript:void(0)" class="w3-button custom-button link-button unlikeButton" data-postid="<?= h($post_id) ?>" data-unlikeid="<?= h($like_id) ?>" data-unlikeurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'unlikePost'])) ?>"><i class="fa fa-thumbs-up"></i> Liked</a>
              <?php
                }
              ?>
              <span id="loadLikeImg<?= h($post_id) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>              
            </div> 
            <div class="w3-col m6 w3-right-align">
              <a id="repostButton<?= h($post_id) ?>" href="javascript:void(0)" class="w3-button custom-button link-button repostButton" data-postid="<?= h($post_id) ?>" data-reposturl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'retweetPost'])) ?>"><i class="fa fa-share"></i> Repost</a>
              <span id="loadRepostImg<?= h($post_id) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif') ?></span>
            </div>
          </div> 
        </div>
        <div class="w3-container w3-padding">
          <?= $this->Form->create($newComment) ?>
            <textarea name="content" required></textarea>
            <?= ($this->Form->isFieldError('content')) ? $this->Form->error('content') : "" ?>
            <div class="w3-row">
              <div class="w3-col m12 w3-right-align">
                <button type="submit" class="w3-button custom-button"><i class="fa fa-comment"></i> Comment</button>
              </div>
            </div>  
          <?= $this->Form->end() ?>
        </div>
        <hr class="w3-clear">
        <?php 
          foreach ($comments as $row) {
        ?>
          <div class="w3-container w3-padding">
            <div class="w3-row">
              <div class="w3-col m6 w3-left-align w3-margin-bottom">
                <?php 
                  $profile_picture = ($row['image_profile']) ? $row['image_profile'] : 'default_avatar.png';
                  echo $this->Html->image('uploads/' . h($profile_picture), ['class' => 'w3-left w3-circle w3-margin-right', 'style' => 'width:60px']); 
                ?>
                <h4><?= h($row['user_name']) ?></h4>
                <small><?= h('@'.$row['username']) ?></small>
              </div>
              <?php 
                if ($row['profile_id'] == $user_id) { 
              ?>
                <div class="w3-col m6 w3-right-align w3-margin-bottom">
                  <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'editComment', 'id' => h($row['comment_id'])])) ?>" class="w3-button custom-button link-button"><i class="fa fa-pencil"></i> Edit</a>
                </div>
              <?php 
                }
              ?>  
              <div class="w3-col m12 w3-container">
                <p><?= nl2br(h($row['content'])) ?></p>
              </div>
            </div>
          </div>
        <?php 
          } 
        ?>
      </div> 
      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <div class="w3-row">
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->prev("Previous Comments") ?></ul>
          </div>
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->counter('Page {{page}} of {{pages}}') ?></ul>
          </div>
          <div class="w3-col m4">
            <ul class="pagination"><?= $this->Paginator->next("Next Comments") ?></ul>
          </div>
        </div>
      </div> 
    </div>
    <!-- End Middle Column -->
    
    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->

  <!-- End Grid -->
  </div>
<!-- End Page Container -->
</div>