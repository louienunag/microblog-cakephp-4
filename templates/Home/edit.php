<div class="w3-container blogTheme">   
  <div class="w3-row">

    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($post_id)])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-reply"></i> Back</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div class="main-nav">    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <?= $this->Form->create($posts, array('enctype'=>'multipart/form-data')) ?>
                <?= $this->Flash->render() ?>
                <input type="text" class="login-input" placeholder="Share your thoughts..." name="content" value="<?= h($posts['content']) ?>">
                <?= ($this->Form->isFieldError('content')) ? $this->Form->error('content') : "" ?>
                <input type="file" id="upload_image" name="edit_image" accept="image/*" onchange="filePreview(this)" hidden>
                <?= ($this->Form->isFieldError('image')) ? $this->Form->error('image') : "" ?>
                <div id="previewImage">
                <?php 
                  if ($posts['image']) {
                ?>
                    Preview: <br />
                    <?= $this->Html->image('uploads/' . h($posts['image']), ['style' => 'width: 50%; height: 50%;']); ?>
                <?php 
                  }  
                ?>
                </div>
                <br />
                <div class="w3-row">
                  <div class="w3-col m6">
                    <button type="button" class="w3-button custom-button" onclick="document.getElementById('upload_image').click();"><i class="fa fa-file-image-o"></i> Add Image</button> 
                    <button type="submit" class="w3-button custom-button"><i class="fa fa-pencil"></i> Update Post</button> 
                  </div>
                  <div class="w3-col m6 w3-right-align">
                    <button id="deletepostButton<?= h($post_id) ?>" type="button" class="w3-button custom-button deletepostButton" data-postid="<?= h($post_id) ?>" data-deleteurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'delete'])) ?>"><i class="fa fa-trash"></i> Delete Post</button> 
                    <span id="loadDeleteImg<?= h($post_id) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif'); ?></span>
                  </div>
                </div>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Middle Column -->

    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['controller' => 'Home', 'action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->
    
  </div>
</div>

<script type="text/javascript">
  var homeUrl = <?= json_encode(h($this->Url->build(['controller' => 'Home', 'action' => 'index']))) ?>;
</script>