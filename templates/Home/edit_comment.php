<div class="w3-container blogTheme">   
  <div class="w3-row">

    <!-- Left Column -->
    <div class="sidenav left-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($comments['post_id'])])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-reply"></i> Back</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Left Column -->

    <!-- Middle Column -->
    <div class="main-nav">    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <?= $this->Form->create($comments, array('enctype'=>'multipart/form-data')) ?>
                <?= $this->Flash->render() ?>
                <input type="text" class="login-input" placeholder="Share your thoughts..." name="content" value="<?= h($comments['content']) ?>">
                <?= ($this->Form->isFieldError('content')) ? $this->Form->error('content') : ""; ?>
                <br />
                <div class="w3-row">
                  <div class="w3-col m6"> 
                    <button type="submit" class="w3-button custom-button"><i class="fa fa-pencil"></i> Update Comment</button> 
                  </div>
                  <div class="w3-col m6 w3-right-align">
                    <button id="deletecommentButton<?= h($comment_id) ?>" type="button" class="w3-button custom-button deletecommentButton" data-commentid="<?= h($comment_id) ?>" data-deleteurl="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'deleteComment'])) ?>"><i class="fa fa-trash"></i> Delete Comment</button> 
                    <span id="loadDeleteImg<?= h($comment_id) ?>" class="loadImg"><?= $this->Html->image('uploads/ajax-loader.gif'); ?></span>
                  </div>
                </div>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Middle Column -->

    <!-- Right Column -->
    <div class="sidenav right-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container w3-padding">
            <?= $this->Form->create(null, ['type' => 'get', 'url' => ['controller' => 'Home', 'action' => 'search']]) ?>
              <input type="hidden" name="searchType" value="Posts">
              <input type="text" class="login-input" placeholder="Search" name="keyword" required>
              <button type="submit" class="w3-button custom-button"><i class="fa fa-search"></i> Search</button> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Right Column -->
    
    <!-- Bottom Left Nav -->
    <div class="bottom-sidenav">
      <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
          <div class="w3-container">
          <a href="<?= h($this->Url->build(['controller' => 'Home', 'action' => 'index'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-home"></i> Home</a>
          <a href="<?= h($this->Url->build(['controller' => 'Login', 'action' => 'logout'])) ?>" class="w3-button w3-block w3-left-align"><i class="fa fa-sign-out"></i> Log Out</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- End Bottom Left Nav -->
    
  </div>
</div>

<script type="text/javascript">
  var homeUrl = <?= json_encode(h($this->Url->build(['controller' => 'Home', 'action' => 'view', 'id' => h($comments['post_id'])]))) ?>;
</script>