<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    <?php $this->assign('title', h($title)); ?>
    <?= $this->fetch('title') ?>
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

  <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>
  <?= $this->Html->css(['normalize.min.css', 'milligram.min.css', 'cake.css', 'DebugKit.toolbar.css', 'microblog.css', 'w3.css', 'w3-theme-blue-grey.css', 'font-awesome.min.css']) ?>
  <?= $this->Html->script(['DebugKit.toolbar.js', 'jquery-3.6.0.min.js', 'microblog.js']) ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>

</head>
<body class="blogTheme">

<div class="main">
  <?= $this->Flash->render() ?>
  <?= $this->fetch('content') ?>
</div>

<?= $this->Html->script('microblog_request.js'); ?>
</body>
</html>